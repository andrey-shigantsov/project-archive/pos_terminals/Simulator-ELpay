/* 
 * File:   transport.h
 */

#include "protocol.h"

#include <boost/asio.hpp>

#ifndef TRANSPORT_H
#define TRANSPORT_H

using namespace boost;
using namespace boost::asio;

bool init_tcp_socket(ip::tcp::socket *This);
bool close_tcp_socket(ip::tcp::socket *This);

bool elpay_reg_req_send(ip::tcp::socket *Socket, ELpayRegData_t* data);
bool elpay_reg_resp_get(ip::tcp::socket *Socket, ELpayTerminalData_t* ThisData, ELpayKey_t regcode);

bool elpay_auth_req_send(ip::tcp::socket *Socket, ELpayAuthData_t* data, const char * sign_key);
bool elpay_auth_resp_get(ip::tcp::socket *Socket, ELpayTerminalData_t* ThisData);

bool elpay_balance_req_send(ip::tcp::socket *Socket, ELpayBalanceData_t* data, const char * sign_key);
bool elpay_balance_resp_get(ip::tcp::socket *Socket, ELpayTerminalData_t* ThisData);

bool elpay_products_list_req_send(ip::tcp::socket *Socket, ELpayProductsListData_t* data, const char * sign_key);
bool elpay_products_list_resp_get(ip::tcp::socket *Socket, ELpayTerminalData_t* ThisData);

bool elpay_receipt_template_req_send(ip::tcp::socket *Socket, ELpayReceiptTemplateData_t* data, const char * sign_key);
bool elpay_receipt_template_resp_get(ip::tcp::socket *Socket, ELpayTerminalData_t* ThisData);

bool elpay_create_operation_req_send(ip::tcp::socket *Socket, ELpayCreateOperationData_t* data, const char * sign_key);
bool elpay_create_operation_resp_get(ip::tcp::socket *Socket, ELpayTerminalData_t* ThisData);

bool elpay_confirm_operation_req_send(ip::tcp::socket *Socket, ELpayConfirmOperationData_t* data, const char * sign_key);
bool elpay_confirm_operation_resp_get(ip::tcp::socket *Socket, ELpayTerminalData_t* ThisData);

bool elpay_operation_status_req_send(ip::tcp::socket *Socket, ELpayOperationStatusData_t* data, const char * sign_key);
bool elpay_operation_status_resp_get(ip::tcp::socket *Socket, ELpayTerminalData_t* ThisData);

#endif /* TRANSPORT_H */

/* 
 * File:   operations.h
 */

#ifndef OPERATIONS_H
#define OPERATIONS_H

#include "protocol.h"
#include "operationStatus.h"

ELpayOperationStatus_t elpay_registration(ELpayTerminalData_t* ThisData, long code);
ELpayOperationStatus_t elpay_authorization(ELpayTerminalData_t* ThisData);
ELpayOperationStatus_t elpay_get_balance(ELpayTerminalData_t* ThisData);
ELpayOperationStatus_t elpay_get_products_list(ELpayTerminalData_t* ThisData);
ELpayOperationStatus_t elpay_get_receipt_template(ELpayTerminalData_t* ThisData, const char * name);
ELpayOperationStatus_t elpay_create_operation(ELpayTerminalData_t* ThisData, long pid);
ELpayOperationStatus_t elpay_confirm_operation(ELpayTerminalData_t* ThisData);
ELpayOperationStatus_t elpay_operation_status(ELpayTerminalData_t* ThisData);

#endif /* OPERATIONS_H */

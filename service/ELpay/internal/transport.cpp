#include "public.h"
#include "transport.h"
#include "ELpay_port.h"

#include "system/logs.h"

using namespace boost;
using namespace boost::system;

bool init_tcp_socket(ip::tcp::socket *This)
{
  try
  {  
    if (flag_Server_ResolveAddr)
    {
      ip::tcp::resolver Resolver(This->get_io_service());
      ip::tcp::resolver::query Query(Server_json_Addr, "http");
      ip::tcp::resolver::iterator Iterator = Resolver.resolve(Query);
      This->connect(*Iterator);
    }
    else
    {
      ip::tcp::endpoint EndPoint(ip::address::from_string(ipPoint_json_Addr), 
                                 ipPoint_json_Port);
      This->connect(EndPoint);
    }

  }
  catch (boost::system::system_error &err)
  {
    LOG_ERR << "(EXCEPTION::Terminal) Init socket: " << err.what();
    return false;
  }
  return true;
}

bool close_tcp_socket(ip::tcp::socket *This)
{
  try
  {
    This->shutdown(ip::tcp::socket::shutdown_receive);
    This->close();
  }
  catch (boost::system::system_error &err)
  {
    LOG_ERR << "(EXCEPTION::Terminal) Close socket: " << err.what();
    return false;
  }
  return true;
}

std::string elpay_http_signature(const char * sign_key, char * jsonBuf)
{
  char * sign = elpay_sign(sign_key, jsonBuf);
  
  std::string SignStr;
  SignStr.append("X-Spek-Signature: ");
  SignStr.append(sign);
  SignStr.append("\r\n");
  
  free(sign);
  return SignStr;
}

//------------------------------------------------------------------------------

bool elpay_transmit(ip::tcp::socket *Socket, char * data, size_t len,
                    const char *  userHeader)
{
  std::string httpString = elpay_http_post(data, len, userHeader);
  
  if (flag_stdout_Verbose)
    LOG_INFO_DATA("ELpay: request: http data:", httpString); 
  
  size_t writeCount = Socket->write_some(buffer(httpString));
  if (writeCount != httpString.size())
  {
    LOG_WARN << "ELpay: write request: count mistmatch";
    return false;
  }
  LOG_INFO << "ELpay: request transmitted";
  return true;  
}

bool elpay_transmit_with_sign(ip::tcp::socket *Socket, char * data, size_t len, const char * sign_key)
{
  std::string httpHeader = elpay_http_signature(sign_key, data);  
  return elpay_transmit(Socket, data, len, httpHeader.data());
}

int elpay_socket_read(ip::tcp::socket *Socket, char * buf, size_t bufSize)
{
  error_code ec;
  int readLen = Socket->read_some(buffer(buf, bufSize),ec);
  if (ec)
  {
    LOG_WARN << "ELpay: socket read: " << ec.message();
    return -1;
  }
  if (readLen <= 0)
  {
    LOG_WARN << "ELpay: socket read: dataLen <= 0"; 
    return -1;
  }  
  return readLen;
}

long elpay_receive(ip::tcp::socket *Socket, char ** content)
{
  LOG_INFO << "ELpay: waiting response...";
  
  const size_t bufSize = 1024;
  char buf[bufSize];
  int readLen = elpay_socket_read(Socket, buf, bufSize);
  if (readLen <= 0)
    return -1;  
  
  long contentLen = elpay_http_content_len(buf, readLen);
  if (contentLen > 0)
  {
    *content = new char[contentLen+1];
  }
  else
  {
    *content = 0;
    LOG_WARN << "ELpay: read http: read content len failure";
    return -1;
  }
  
  int contentIdx = elpay_http_content_idx(buf, readLen);
  if (contentIdx < 0)
  {
    LOG_WARN << "ELpay: read http: read content idx failure";
    return -1;
  }
  
  long contentCounter = readLen - contentIdx;
  strncpy(*content, &buf[contentIdx], contentCounter);
  while (contentCounter < contentLen)
  {
    readLen = elpay_socket_read(Socket, &(*content)[contentCounter],contentLen-contentCounter);
    if (readLen < 0)
      return -1;    
    contentCounter += readLen;
  }
  (*content)[contentLen] = '\0';
  
  if (flag_stdout_Verbose)
    LOG_INFO_DATA("ELpay: response data:",
      std::string(buf,contentIdx) << *content);
  
  LOG_INFO << "ELpay: response received";

  return contentLen;
}

//------------------------------------------------------------------------------

#define REQ_SEND_LOG_HEADER "ELpay: request: " REQ_SEND_OPERATION

#define REQ_SEND_CHECK_AND_RETURN(x,msg) \
  if (x) \
  { \
    LOG_INFO << REQ_SEND_LOG_HEADER ": " msg; \
    return false; \
  }

#define REQ_SEND_BASE \
  const size_t jsonBufSize = 65536; \
  char jsonBuf[jsonBufSize]; \
  size_t jsonBufLen;

#define REQ_SEND_CHECK_GEN_AND_RETURN REQ_SEND_CHECK_AND_RETURN(jsonBufLen <= 0, "generation failure");

bool elpay_reg_req_send(ip::tcp::socket *Socket, ELpayRegData_t* data)
{
  #define REQ_SEND_OPERATION "registration"
  REQ_SEND_BASE
  
  LOG_INFO << REQ_SEND_LOG_HEADER " preparing with code "
    << data->code << "...";
  
  jsonBufLen = elpay_reg_req_gen(data, jsonBuf, jsonBufSize);
  REQ_SEND_CHECK_GEN_AND_RETURN

  return elpay_transmit(Socket, jsonBuf, jsonBufLen, "");
  #undef REQ_SEND_OPERATION
}

bool elpay_auth_req_send(ip::tcp::socket *Socket, ELpayAuthData_t* data, 
  const char * sign_key)
{  
  #define REQ_SEND_OPERATION "authorization"
  REQ_SEND_BASE

  LOG_INFO << REQ_SEND_LOG_HEADER ": preparing...";
  
  jsonBufLen = elpay_auth_req_gen(data, jsonBuf, jsonBufSize);
  REQ_SEND_CHECK_GEN_AND_RETURN
  
  return elpay_transmit_with_sign(Socket, jsonBuf, jsonBufLen, sign_key);
  #undef REQ_SEND_OPERATION
}

bool elpay_products_list_req_send(ip::tcp::socket *Socket,
    ELpayProductsListData_t* data,
    const char * sign_key)
{
  #define REQ_SEND_OPERATION "Product List"
  REQ_SEND_BASE

  LOG_INFO << REQ_SEND_LOG_HEADER ": preparing...";
  
  jsonBufLen = elpay_products_list_req_gen(data, jsonBuf, jsonBufSize);
  REQ_SEND_CHECK_GEN_AND_RETURN

  return elpay_transmit_with_sign(Socket, jsonBuf, jsonBufLen, sign_key);
  #undef REQ_SEND_OPERATION
}

bool elpay_receipt_template_req_send(ip::tcp::socket *Socket,
    ELpayReceiptTemplateData_t* data,
    const char * sign_key)
{
  #define REQ_SEND_OPERATION "Receipt Template"
  REQ_SEND_BASE

  LOG_INFO << REQ_SEND_LOG_HEADER ": preparing...";
  
  jsonBufLen = elpay_receipt_template_req_gen(data, jsonBuf, jsonBufSize);
  REQ_SEND_CHECK_GEN_AND_RETURN

  return elpay_transmit_with_sign(Socket, jsonBuf, jsonBufLen, sign_key);
  #undef REQ_SEND_OPERATION
}

bool elpay_create_operation_req_send(ip::tcp::socket *Socket,
    ELpayCreateOperationData_t* data,
    const char * sign_key)
{
  #define REQ_SEND_OPERATION "Create Operation"
  REQ_SEND_BASE

  LOG_INFO << REQ_SEND_LOG_HEADER ": preparing...";
  
  jsonBufLen = elpay_create_operation_req_gen(data, jsonBuf, jsonBufSize);
  REQ_SEND_CHECK_GEN_AND_RETURN

  return elpay_transmit_with_sign(Socket, jsonBuf, jsonBufLen, sign_key);
  #undef REQ_SEND_OPERATION
}

bool elpay_confirm_operation_req_send(ip::tcp::socket *Socket,
    ELpayConfirmOperationData_t* data,
    const char * sign_key)
{
  #define REQ_SEND_OPERATION "Confirm Operation"
  REQ_SEND_BASE

  LOG_INFO << REQ_SEND_LOG_HEADER ": preparing...";
  
  jsonBufLen = elpay_confirm_operation_req_gen(data, jsonBuf, jsonBufSize);
  REQ_SEND_CHECK_GEN_AND_RETURN

  return elpay_transmit_with_sign(Socket, jsonBuf, jsonBufLen, sign_key);
  #undef REQ_SEND_OPERATION
}

bool elpay_operation_status_req_send(ip::tcp::socket *Socket,
    ELpayOperationStatusData_t* data,
    const char * sign_key)
{
  #define REQ_SEND_OPERATION "Operation Status"
  REQ_SEND_BASE

  LOG_INFO << REQ_SEND_LOG_HEADER ": preparing...";
  
  jsonBufLen = elpay_operation_status_req_gen(data, jsonBuf, jsonBufSize);
  REQ_SEND_CHECK_GEN_AND_RETURN

  return elpay_transmit_with_sign(Socket, jsonBuf, jsonBufLen, sign_key);
  #undef REQ_SEND_OPERATION
}

bool elpay_balance_req_send(ip::tcp::socket *Socket,
    ELpayBalanceData_t* data,
    const char * sign_key)
{
  #define REQ_SEND_OPERATION "Balance"
  REQ_SEND_BASE

  LOG_INFO << REQ_SEND_LOG_HEADER ": preparing...";
  
  jsonBufLen = elpay_balance_req_gen(data, jsonBuf, jsonBufSize);
  REQ_SEND_CHECK_GEN_AND_RETURN

  return elpay_transmit_with_sign(Socket, jsonBuf, jsonBufLen, sign_key);
  #undef REQ_SEND_OPERATION
}

//------------------------------------------------------------------------------

#define RESP_GET_LOG_HEADER "ELpay: response: " RESP_GET_OPERATION

#define RESP_GET_RETURN(res) \
do { \
  delete [] buf; \
  return res; \
} while(0)

#define RESP_GET_BASE \
  char * buf; \
  int bufLen = elpay_receive(Socket, &buf); \
  if (bufLen < 0) \
    RESP_GET_RETURN(false); \
   \
  char jsonErrBuf[1024]; \
  int res;

#define RESP_GET_CHECK_PARSE_RES_AND_RETURN \
  if (!res) \
  { \
    LOG_WARN << RESP_GET_LOG_HEADER << ": JSON: " << jsonErrBuf; \
    RESP_GET_RETURN(false); \
  }

bool elpay_reg_resp_get(ip::tcp::socket *Socket, ELpayTerminalData_t* ThisData, ELpayKey_t regcode)
{
  #define RESP_GET_OPERATION "registration"
  RESP_GET_BASE
  
  res = elpay_reg_resp_parse(buf, regcode, ThisData,
                                 jsonErrBuf,sizeof(jsonErrBuf));
  RESP_GET_CHECK_PARSE_RES_AND_RETURN
  
  char * encSK;
  elpay_base64_encode(ThisData->sign_key, ELPAY_JSON_KEYSIZE, &encSK);
  LOG_INFO << RESP_GET_LOG_HEADER ": successful ("
    << "TermId=" << ThisData->id
    << " " 
    << "SignKey=" << encSK << ")";
  free(encSK);
  
  RESP_GET_RETURN(true);
  #undef RESP_GET_OPERATION
}

bool elpay_auth_resp_get(ip::tcp::socket *Socket, ELpayTerminalData_t* ThisData)
{
  #define RESP_GET_OPERATION "authorization"
  RESP_GET_BASE
  
  res = elpay_auth_resp_parse(buf, ThisData,
                                  jsonErrBuf,sizeof(jsonErrBuf));
  RESP_GET_CHECK_PARSE_RES_AND_RETURN
  
  LOG_INFO << RESP_GET_LOG_HEADER ": successful ("
    << "SessionId=" << ThisData->session << ")";

  RESP_GET_RETURN(true);
  #undef RESP_GET_OPERATION
}

bool elpay_products_list_resp_get(ip::tcp::socket *Socket, ELpayTerminalData_t* ThisData)
{
  #define RESP_GET_OPERATION "Products List"
  RESP_GET_BASE
    
//  res = elpay_auth_resp_parse(buf, ThisData,
//                                  jsonErrBuf,sizeof(jsonErrBuf));
//  RESP_GET_CHECK_PARSE_RES_AND_RETURN
    
  LOG_INFO << RESP_GET_LOG_HEADER ": successful";

  RESP_GET_RETURN(true);
  #undef RESP_GET_OPERATION
}

bool elpay_receipt_template_resp_get(ip::tcp::socket *Socket, ELpayTerminalData_t* ThisData)
{
  #define RESP_GET_OPERATION "Products List"
  RESP_GET_BASE
    
//  res = elpay_auth_resp_parse(buf, ThisData,
//                                  jsonErrBuf,sizeof(jsonErrBuf));
//  RESP_GET_CHECK_PARSE_RES_AND_RETURN
    
  LOG_INFO << RESP_GET_LOG_HEADER ": successful";

  RESP_GET_RETURN(true);
  #undef RESP_GET_OPERATION
}

bool elpay_create_operation_resp_get(ip::tcp::socket *Socket, ELpayTerminalData_t* ThisData)
{
  #define RESP_GET_OPERATION "Products List"
  RESP_GET_BASE
    
  res = elpay_create_operation_resp_parse(buf, ThisData,
                                  jsonErrBuf,sizeof(jsonErrBuf));
  RESP_GET_CHECK_PARSE_RES_AND_RETURN
    
  LOG_INFO << RESP_GET_LOG_HEADER ": successful";

  RESP_GET_RETURN(true);
  #undef RESP_GET_OPERATION
}

bool elpay_balance_resp_get(ip::tcp::socket *Socket, ELpayTerminalData_t* ThisData)
{
  #define RESP_GET_OPERATION "Balance"
  RESP_GET_BASE
    
//  res = elpay_auth_resp_parse(buf, ThisData,
//                                  jsonErrBuf,sizeof(jsonErrBuf));
//  RESP_GET_CHECK_PARSE_RES_AND_RETURN
    
  LOG_INFO << RESP_GET_LOG_HEADER ": successful";

  RESP_GET_RETURN(true);
  #undef RESP_GET_OPERATION
}

bool elpay_confirm_operation_resp_get(ip::tcp::socket *Socket, ELpayTerminalData_t* ThisData)
{
  #define RESP_GET_OPERATION "Confirm Operation"
  RESP_GET_BASE
    
//  res = elpay_auth_resp_parse(buf, ThisData,
//                                  jsonErrBuf,sizeof(jsonErrBuf));
//  RESP_GET_CHECK_PARSE_RES_AND_RETURN
    
  LOG_INFO << RESP_GET_LOG_HEADER ": successful";

  RESP_GET_RETURN(true);
  #undef RESP_GET_OPERATION
}

bool elpay_operation_status_resp_get(ip::tcp::socket *Socket, ELpayTerminalData_t* ThisData)
{
  #define RESP_GET_OPERATION "Operation Status"
  RESP_GET_BASE
    
//  res = elpay_auth_resp_parse(buf, ThisData,
//                                  jsonErrBuf,sizeof(jsonErrBuf));
//  RESP_GET_CHECK_PARSE_RES_AND_RETURN
    
  LOG_INFO << RESP_GET_LOG_HEADER ": successful";

  RESP_GET_RETURN(true);
  #undef RESP_GET_OPERATION
}

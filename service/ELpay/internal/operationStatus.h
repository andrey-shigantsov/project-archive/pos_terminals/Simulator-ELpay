/* 
 * File:   operationStatus.h
 */

#ifndef ELPAY_OPERATIONSTATUS_H
#define ELPAY_OPERATIONSTATUS_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum
{
  elpay_status_OK,
  elpay_status_UnsupportedOperation,
  elpay_status_GeneralFailure,
  
  elpay_status_RequestFailure,
  elpay_status_ResponseFailure
} ELpayOperationStatus_t;

const char * elpay_operation_status(ELpayOperationStatus_t status);

#ifdef __cplusplus
}
#endif

#endif /* ELPAY_OPERATIONSTATUS_H */


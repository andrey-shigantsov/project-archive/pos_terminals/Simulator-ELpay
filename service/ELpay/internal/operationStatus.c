/* 
 * File:   operationStatus.c
 */

#include "operationStatus.h"

static const char * statusText[] =
{
  "Successful",
  "Unsupported Operation",
  "General Failure",
  
  "Request Failure",
  "Response Failure"
};

const char * elpay_operation_status(ELpayOperationStatus_t status)
{
  return statusText[status];
}
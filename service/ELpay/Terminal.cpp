/* 
 * File:   Terminal.c
 */

#include "public.h"
#include "Terminal.h"

#include "internal/operations.h"
#include "ELpay_port.h"

#include "system/logs.h"

#include <boost/thread.hpp>
#include <boost/interprocess/ipc/message_queue.hpp>

#include <boost/algorithm/string.hpp>

using namespace boost;
using namespace boost::system;

static ELpayTerminalData_t ThisData;

static thread * Terminal = 0;
static boost::interprocess::message_queue * TerminalQueue, * TerminalResultQueue;

void Terminal_main();
void init_Terminal()
{
  LOG_INFO << "Init Terminal...";
  
  interprocess::message_queue::remove(TERMINAL_QUEUE_NAME);
  TerminalQueue = 
    new interprocess::message_queue(interprocess::open_or_create, TERMINAL_QUEUE_NAME, 
                                    TERMINAL_QUEUE_MAXCOUNT, TERMINAL_QUEUE_MSGSIZE);
  TerminalResultQueue =
    new interprocess::message_queue(interprocess::open_or_create, TERMINAL_RES_QUEUE_NAME, 
                                    TERMINAL_RES_QUEUE_MAXCOUNT, TERMINAL_RES_QUEUE_MSGSIZE);    
  Terminal = new thread(Terminal_main);
}

void free_Terminal()
{
  Terminal_exit();
  Terminal->join();
  
  delete TerminalQueue;
  interprocess::message_queue::remove(TERMINAL_QUEUE_NAME);

  delete TerminalResultQueue;
  interprocess::message_queue::remove(TERMINAL_RES_QUEUE_NAME);
  
  delete Terminal;
}

bool Terminal_setAuthData(std::string & data)
{
  std::vector<std::string> x;
  split(x, data, is_any_of(":"));
  if (x.size() != 2)
  {
    LOG_INFO << "Terminal: auth data incorrect";
    return false;
  }
  
  strcpy(ThisData.login, x[0].data());
  strcpy(ThisData.password, x[1].data());
  LOG_INFO << "Terminal: using auth " << data;
}

void Terminal_setId(const char * id)
{
  strncpy(ThisData.id, id, ELPAY_JSON_TERM_ID_BUFSIZE);
  LOG_INFO << "Terminal: using id " << id;
}

bool Terminal_setSignKey(const char * sign_key)
{
  if (strlen(sign_key) != ELPAY_JSON_KEYenc_SIZE)
  {
    LOG_WARN << "Terminal: Sign Key: size incorrect";
    return false;
  }
  
  char * sk;
  size_t len = elpay_base64_decode(sign_key, ELPAY_JSON_KEYenc_SIZE, &sk);
  if (len)
  {
    memcpy(ThisData.sign_key, sk, ELPAY_JSON_KEYSIZE);
    ThisData.sign_key[ELPAY_JSON_KEYSIZE] = '\0';
  }
  else
  {
    LOG_WARN << "Terminal: Sign Key: decode failure";
    return false;
  }
  free(sk);
  
  LOG_INFO << "Terminal: using sign key " << sign_key;
  return true;
}

void Terminal_exit()
{
  TerminalMsg msg;
  msg.action = TerminalMsg::Exit;  
  TerminalQueue->send(&msg, sizeof(msg), 0);  
}

void Terminal_elpay_reg(long code)
{ 
  TerminalMsg msg;
  msg.action = TerminalMsg::ELpay_Reg;
  msg.data.reg.code = code;
  TerminalQueue->send(&msg, sizeof(msg), 0);
}

void Terminal_elpay_auth()
{ 
  TerminalMsg msg;
  msg.action = TerminalMsg::ELpay_Auth;
  TerminalQueue->send(&msg, sizeof(msg), 0);
}

void Terminal_elpay_getProductsList()
{
  TerminalMsg msg;
  msg.action = TerminalMsg::ELpay_ProductsList;
  TerminalQueue->send(&msg, sizeof(msg), 0);
}

void Terminal_elpay_getReceiptTemplate(const char * name)
{
  TerminalMsg msg;
  msg.action = TerminalMsg::ELpay_ReceiptTemplate;
  strncpy(msg.data.recTemplName, name, sizeof(TerminalReceiptTemplateNameBuf_t));
  TerminalQueue->send(&msg, sizeof(msg), 0);
}

void Terminal_elpay_CreateOperation(long pid)
{
  TerminalMsg msg;
  msg.action = TerminalMsg::ELpay_CreateOperation;
  msg.data.createOp.pid = pid;
  TerminalQueue->send(&msg, sizeof(msg), 0);  
}

void Terminal_elpay_ConfirmOperation()
{
  TerminalMsg msg;
  msg.action = TerminalMsg::ELpay_ConfirmOperation;
  TerminalQueue->send(&msg, sizeof(msg), 0);  
}

void Terminal_elpay_OperationStatus()
{
  TerminalMsg msg;
  msg.action = TerminalMsg::ELpay_OperationStatus;
  TerminalQueue->send(&msg, sizeof(msg), 0);  
}

void Terminal_elpay_getBalance()
{
  TerminalMsg msg;
  msg.action = TerminalMsg::ELpay_Balance;
  TerminalQueue->send(&msg, sizeof(msg), 0);
}

bool Terminsl_getResultMsg(TerminalResultMsg * msg)
{
  interprocess::message_queue queue(interprocess::open_only,TERMINAL_RES_QUEUE_NAME);
  
  unsigned int priority;
  std::size_t recvd_size;
  msg->status = elpay_status_GeneralFailure;
  queue.receive(msg, sizeof(TerminalResultMsg), recvd_size, priority);
  
  return true;
}

bool Terminsl_getResultStr(std::string * str)
{
  interprocess::message_queue queue(interprocess::open_only,TERMINAL_RES_QUEUE_NAME);
  
  TerminalResultMsg msg;
  unsigned int priority;
  std::size_t recvd_size;
  msg.status = elpay_status_GeneralFailure;  
  queue.receive(&msg, sizeof(msg), recvd_size, priority);
  
  str->clear();
  str->append(elpay_operation_status(msg.status));
  return true;
}

//------------------------------------------------------------------------------

static void send_result(ELpayOperationStatus_t status)
{
  TerminalResultMsg msg;
  msg.status = status;
  TerminalResultQueue->send(&msg, sizeof(msg), 0);
}

void Terminal_main()
{
  LOG_INFO << "Terminal: started";
  try
  {
    interprocess::message_queue queue(interprocess::open_only,TERMINAL_QUEUE_NAME);
    
    TerminalMsg msg;
    unsigned int priority;
    std::size_t recvd_size;
    do
    {
      msg.action = TerminalMsg::Exit;
      queue.receive(&msg, sizeof(msg), recvd_size, priority);
      
      ELpayOperationStatus_t res = elpay_status_UnsupportedOperation;
      switch(msg.action)
      {
      default: break;
      case TerminalMsg::ELpay_Reg:
        res = elpay_registration(&ThisData, msg.data.reg.code);
        break;
      case TerminalMsg::ELpay_Auth:
        res = elpay_authorization(&ThisData);
        break;
      case TerminalMsg::ELpay_ProductsList:
        res = elpay_get_products_list(&ThisData);
        break;
      case TerminalMsg::ELpay_ReceiptTemplate:
        res = elpay_get_receipt_template(&ThisData, msg.data.recTemplName);
        break;
      case TerminalMsg::ELpay_CreateOperation:
        res = elpay_create_operation(&ThisData, msg.data.createOp.pid);
        break;
      case TerminalMsg::ELpay_ConfirmOperation:
        res = elpay_confirm_operation(&ThisData);
        break;
      case TerminalMsg::ELpay_OperationStatus:
        ThisData.tranId = 2016161583;
        res = elpay_operation_status(&ThisData);
        break;
      case TerminalMsg::ELpay_Balance:
        res = elpay_get_balance(&ThisData);
        break;
      }
      send_result(res);
    } while (msg.action);
  }
  catch (interprocess::interprocess_exception &e)
  {
    LOG_ERR << "(EXCEPTION::Terminal) "  << e.what();
  }
  LOG_INFO << "Terminal: finished";
}

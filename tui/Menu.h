#ifndef MENU_H
#define MENU_H

enum menuAction_t
{
  menu_NoAction = -1,
  menu_Exit,
  menu_ELpay_Reg,
  menu_ELpay_Auth,
  menu_ELpay_Balance,
  menu_ELpay_ProductsList,
  menu_ELpay_ReceiptTemplate,
  menu_ELpay_CreateOperation,
  menu_ELpay_ConfirmOperation,
  menu_ELpay_OperationStatus
};

menuAction_t menu_handler(bool isAuth);

#endif // MENU_H
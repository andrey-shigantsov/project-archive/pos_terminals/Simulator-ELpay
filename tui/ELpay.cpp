#include "public.h"

#include <cstdlib>
#include <iostream>

long elpayRegCode()
{
  if (elpayRCode)
    return elpayRCode;
  
  std::cout << "Enter registration code: ";
  long x;
  std::cin >> x;
  return x;
}

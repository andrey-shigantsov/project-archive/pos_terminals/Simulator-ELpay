/* 
 * File:   json.h
 */

#ifndef JSON_H
#define JSON_H

#include <yajl/yajl_gen.h>
#include <yajl/yajl_tree.h>


#ifdef  __cplusplus
extern "C" {
#endif

#define JSON_CHAR unsigned char
  
typedef struct
{
  yajl_gen Generator;
} jsonGen_t;

void init_jsonGen(jsonGen_t* This);
void free_jsonGen(jsonGen_t* This);

void jsonGen_clear(jsonGen_t* This);

int json_gen_object_open(jsonGen_t* This, const JSON_CHAR * name);
int json_gen_object_close(jsonGen_t* This);

int json_gen_array_open(jsonGen_t* This, const JSON_CHAR * name);
int json_gen_array_close(jsonGen_t* This);

int json_gen_write_null(jsonGen_t* This, const JSON_CHAR * name);
int json_gen_write_str(jsonGen_t* This, const JSON_CHAR * name, const JSON_CHAR * str);
int json_gen_write_llint(jsonGen_t* This, const JSON_CHAR * name, long long val);

int jsonGen_internal_buf(jsonGen_t* This, const JSON_CHAR ** buf, size_t * len);
size_t jsonGen_copy_to_buf(jsonGen_t* This, char * buf, size_t size);

#ifdef  __cplusplus
}
#endif

#endif /* JSON_H */

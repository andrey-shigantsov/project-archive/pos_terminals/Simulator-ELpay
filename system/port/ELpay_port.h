/* 
 * File:   json_ELpay_port.h
 */

#include <stdint.h>
#include <stddef.h>

#ifndef JSON_ELPAY_PORT_H
#define JSON_ELPAY_PORT_H

#ifdef __cplusplus
extern "C"
{
#endif

extern uint16_t elpay_crc16(const char * bytes, size_t count);
  
extern int elpay_des_encrypt(const char * key, const char * bytes, size_t count, char * res);
extern int elpay_des_decrypt(const char * key, const char * bytes, size_t count, char * res);

extern size_t elpay_base64_encode(const char * bytes, size_t count, char ** res);
extern size_t elpay_base64_decode(const char * bytes, size_t count, char ** res);

#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
std::string elpay_http_post(char * data, size_t len,
                     const char * userHeader);
long elpay_http_content_len(const char * buf, size_t len);
int elpay_http_content_idx(const char * buf, size_t len);
#endif

#endif /* JSON_ELPAY_PORT_H */


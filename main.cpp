/* 
 * File:   main.cpp
 */

#include "public.h"
#include "app_options.h"

#include "system/logs.h"

#include "tui/Menu.h"
#include "tui/ELpay.h"

#include "etc/DummyServer.h"
#include "service/ELpay/Terminal.h"

#include <cstdlib>
#include <iostream>

#include <boost/thread.hpp>
#include <boost/interprocess/ipc/message_queue.hpp>

using namespace boost;

//------------------------------------------------------------------------------
void init_modules();
void free_modules();

int main(int argc, char** argv)
{
  init_system_logs();
  
  menuAction_t menuAction = menu_Exit;
  appCommand_t cmd = app_options_handler(argc, argv, &menuAction);
  if (cmd == app_EXIT) return 0;
 
  //----------------------------------------------------------------------------
  
  init_modules();
  
  int res = 0;
  std::string operationName, opResult;
  try
  {
    bool isRunning = true;
    do
    {
      if (flag_readAction_enable)
        menuAction = menu_handler(flag_isAuth);
      switch(menuAction)
      {
      default:
        break;
        
      case menu_Exit:
        isRunning = false;
        break;

      case menu_ELpay_Reg:{
        operationName = "ELpay Registration";
        if (flag_DummyServer_Enable)
          DummyServer_accept(dummy_accept_Reg);
        Terminal_elpay_reg(elpayRegCode());
        Terminsl_getResultStr(&opResult);
        break;}
      
      case menu_ELpay_Auth:{
        operationName = "ELpay Authorization";
        if (flag_DummyServer_Enable)
          DummyServer_accept(dummy_accept_Auth);
        Terminal_elpay_auth();
        TerminalResultMsg resMsg;
        Terminsl_getResultMsg(&resMsg);
        flag_isAuth = resMsg.status == elpay_status_OK;
        opResult = elpay_operation_status(resMsg.status);
        break;}
      
      case menu_ELpay_Balance:{
        operationName = "Get ELpay Balance";
        if (flag_DummyServer_Enable)
          DummyServer_accept(dummy_accept_Balance);
        Terminal_elpay_getBalance();
        Terminsl_getResultStr(&opResult);        
        break;}
      
      case menu_ELpay_ProductsList:{
        operationName = "ELpay Products List download";
        if (flag_DummyServer_Enable)
          DummyServer_accept(dummy_accept_ProdList);
        Terminal_elpay_getProductsList();
        Terminsl_getResultStr(&opResult);
        break;}
      
      case menu_ELpay_ReceiptTemplate:{
        operationName = "ELpay Receipt Template download";
        if (flag_DummyServer_Enable)
          DummyServer_accept(dummy_accept_RecTempl);
        Terminal_elpay_getReceiptTemplate("gen.prn");
        Terminsl_getResultStr(&opResult);
        break;}
      
      case menu_ELpay_CreateOperation:{
        operationName = "ELpay Create Operation";
        if (flag_DummyServer_Enable)
          DummyServer_accept(dummy_accept_CreOp);
        Terminal_elpay_CreateOperation(1009);
        Terminsl_getResultStr(&opResult);
        break;}

      case menu_ELpay_ConfirmOperation:{
        operationName = "ELpay Confirm Operation";
        if (flag_DummyServer_Enable)
          DummyServer_accept(dummy_accept_ConOp);
        Terminal_elpay_ConfirmOperation();
        Terminsl_getResultStr(&opResult);
        break;}
      
      case menu_ELpay_OperationStatus:{
        operationName = "ELpay Operation Status";
        if (flag_DummyServer_Enable)
          DummyServer_accept(dummy_accept_OpStatus);
        Terminal_elpay_OperationStatus();
        Terminsl_getResultStr(&opResult);
        break;}
      }
      if (menuAction > menu_Exit)
        std::cout << operationName << ": " << opResult << std::endl;
      if (flag_AutoExit)
        isRunning = false;
      else
        flag_readAction_enable = true;
    } while (isRunning);
  }
  catch (std::exception &e)
  {
    LOG_ERR << "(EXCEPTION) "  << e.what();
    res = -1;
  }
  
  free_modules();
  return res;
}

//------------------------------------------------------------------------------
void init_modules()
{
  if (flag_DummyServer_Enable)
  {
    init_DummyServer();
    this_thread::sleep(boost::posix_time::microsec(100));
  }
  init_Terminal();
}

void free_modules()
{
  free_Terminal();
  if (flag_DummyServer_Enable)
    free_DummyServer();
}
